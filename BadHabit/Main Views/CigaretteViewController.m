//
//  CigaretteViewController.m
//  BadHabit
//
//  Created by Arinc Elhan on 28/03/15.
//  Copyright (c) 2015 Reengen. All rights reserved.
//

#import "CigaretteViewController.h"
#import "TopBar.h"
#import "TobaccoViewCell.h"

@interface CigaretteViewController ()
{
    NSMutableArray *loadData;
}
@end

@implementation CigaretteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    loadData = [NSMutableArray new];
    [self autoTobaccoGenerator];
    TopBar* top = [[TopBar alloc] initTopBar:@"Tobacco"];
    top.frame = topCenter.frame;
    [self.view addSubview:top];
}

- (void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [table reloadData];
}

- (void) autoTobaccoGenerator
{
    NSArray * brands = @[@"Marlboro ", @"Parliament ",@"Camel "];
    NSArray * types  = @[@"Light", @"Soft",@"Box"];
    
    for (int i = 0; i<10; i++)
    {
        NSDictionary *dict = @{@"pic_name":@"smoke.png",
                               @"brand":[NSString stringWithFormat:@"%@%@",[brands objectAtIndex:arc4random()%3],[types objectAtIndex:arc4random()%3]],
                               @"price":[NSString stringWithFormat:@"%f",arc4random()%6+4.5],
                               @"count":[NSNumber numberWithInt:arc4random() %20]};
        [loadData addObject:dict];
    }
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [loadData count];
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *stringName = @"TobaccoViewCell";
    
    TobaccoViewCell *cell = (TobaccoViewCell *)[tableView dequeueReusableCellWithIdentifier:stringName];
    
    if (cell == nil)
    {
        // Eğer ayrı bir xib file varsa tableCell için bu yapıyı kullan
        //NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TobaccoViewCell" owner:self options:nil];
        //cell = [nib objectAtIndex:0];
        // Yoksa bunu kullanabilirsin
        //cell = [[TobaccoViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:stringName];
        
        [tableView registerNib:[UINib nibWithNibName:stringName bundle:nil] forCellReuseIdentifier:stringName];
        cell = [tableView dequeueReusableCellWithIdentifier:stringName];
    }
    
    NSDictionary* dict = [loadData objectAtIndex:indexPath.row];
    
    [cell.pic_name setImage:[UIImage imageNamed:[dict objectForKey:@"pic_name"]]];
    [cell.brand setText:[dict objectForKey:@"brand"]];
    [cell.price setText:[dict objectForKey:@"price"]];
    [cell.count setText:[NSString stringWithFormat:@"%d", [[dict objectForKey:@"count"] intValue]]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

@end
