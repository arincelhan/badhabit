//
//  StatViewController.m
//  BadHabit
//
//  Created by Arinc Elhan on 28/03/15.
//  Copyright (c) 2015 Reengen. All rights reserved.
//

#import "StatViewController.h"
#import "TopBar.h"

@interface StatViewController ()

@end

@implementation StatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    TopBar* top = [[TopBar alloc] initTopBar:@"Statistics"];
    top.frame = topCenter.frame;
    [self.view addSubview:top];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}



@end
